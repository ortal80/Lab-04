# CI/CD with Microsoft VSTS
Lab 04: Create your first Release Pipeline

---

## Preparation

 - Configure a second build agent in a different pool (sela-env)
 
 
## Instructions


### Create a release pipeline definition

 - Browse to "Build & Release" page
 
 - Click "+" and then "Create a release pipeline"


### Configure artifacts

 - Configure a single artifact as below:

```
Source Type: Build
Project: <your team project>
Source Build: npm-artifactory
Source alias: _npm-artifactory
```

<img alt="Image 1.1" src="images/task-1.1.png"  width="75%" height="75%">

 - Configure the artifact trigger:

```
Continuous deployment trigger: Enabled
```

<img alt="Image 1.2" src="images/task-1.2.png"  width="75%" height="75%">

### Configure the first environment (Dev)

 - Access to the environment configuration:


<img alt="Image 1.3" src="images/task-1.3.png"  width="75%" height="75%">


 - Configure the Stage name:

```
Stage Name: Dev
```

<img alt="Image 1.4" src="images/task-1.4.png"  width="75%" height="75%">


 - Configure the Agent phase:

```
Display name: Deployment
Agent queue: Sela
```

<img alt="Image 1.5" src="images/task-1.5.png"  width="75%" height="75%">


 - Configure task "Powershell"

```
Task: Powershell
Display name: Stop Services
Type: Inline
Script:

if((Get-Process).ProcessName.Contains("cmd")){Stop-Process -Name cmd}
Start-Sleep -Seconds 3
Remove-Item -Path C:\npm-app\* -Force -Recurse
```

<img alt="Image 1.5" src="images/task-1.5.png"  width="75%" height="75%">

- Configure task "Extract Files"

```
Display name: Extract Files
Archive file patterns: *.zip
Destination folder: "C:\npm-app"
```

<img alt="Image 1.6" src="images/task-1.6.png"  width="75%" height="75%">

 - Configure task "npm"

```
Display name: npm install
Command: custom
Working folder with package.json: "C:\npm-app"
Command and arguments: install
```

<img alt="Image 1.7" src="images/task-1.7.png"  width="75%" height="75%">

 - Configure task "powershell"

```
Display name: start
Type: Inline
Script: Write-Host "Ready to start in C:\npm-app"
```

<img alt="Image 1.8" src="images/task-1.8.png"  width="75%" height="75%">


### Configure the second environment (Production) - Optional

 - Configure the Agent phase:

```
Display name: Deployment
Agent queue: sela-env
```

 - Configure task "Powershell"

```
Task: Powershell
Display name: Stop Services
Type: Inline
Script:

if((Get-Process).ProcessName.Contains("cmd")){Stop-Process -Name cmd}
Start-Sleep -Seconds 3
Remove-Item -Path C:\npm-app\* -Force -Recurse
```

<img alt="Image 1.5" src="images/task-1.5.png"  width="75%" height="75%">

- Configure task "Extract Files"

```
Display name: Extract Files
Archive file patterns: *.zip
Destination folder: "C:\npm-app"
```

<img alt="Image 1.6" src="images/task-1.6.png"  width="75%" height="75%">

 - Configure task "npm"

```
Display name: npm install
Command: custom
Working folder with package.json: "C:\npm-app"
Command and arguments: install
```

<img alt="Image 1.7" src="images/task-1.7.png"  width="75%" height="75%">

 - Configure task "powershell"

```
Display name: start
Type: Inline
Script: Write-Host "Ready to start in C:\npm-app"
```

<img alt="Image 1.8" src="images/task-1.8.png"  width="75%" height="75%">


### Set Pre-Deployment Condition for "Production"

 - Configure the Pre-Deployment Condition:

```
Pre-deployment approvals: Enabled
Approvers: <your-user>
```

